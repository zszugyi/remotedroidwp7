﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Bespoke.Common.Osc;
using System.IO.IsolatedStorage;

namespace RemoteDroidClient
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            RestoreSettings();
        }

        private void connectButton_Click(object sender, RoutedEventArgs e)
        {
            ConnectToServer();
        }

        private void ConnectToServer()
        {
            if (connectLabel.Text.Length > 0)
            {
                OscMessage.LittleEndianByteOrder = false;

                try
                {
                    Client.CLIENT = new Client(serverAddressField.Text);
                    
                    SaveSettings();
                    NavigationService.Navigate(new Uri("/MainControlPage.xaml", UriKind.Relative));
                }
                catch (FormatException ex)
                {
                    MessageBox.Show("Enter a a valid IP address (e.g. 192.168.1.2)");
                }
            }
        }

        private void SaveSettings()
        {
            var settings = IsolatedStorageSettings.ApplicationSettings;
            if (!settings.Contains("server.ip"))
            {
                settings.Add("server.ip", serverAddressField.Text);
            }
            else
            {
                settings["server.ip"] = serverAddressField.Text;
            }
        }

        private void RestoreSettings()
        {
            var settings = IsolatedStorageSettings.ApplicationSettings;
            string serverIp;
            if (!settings.TryGetValue<string>("server.ip", out serverIp))
            {
                serverIp = "192.168.1.";
            }
            serverAddressField.Text = serverIp;

        }

        private void serverAddressField_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ConnectToServer();
            }
        }

    }
}