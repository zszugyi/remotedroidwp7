﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Bespoke.Common.Osc;
using System.Collections.Generic;
using RemoteDroidClient;

namespace RemoteDroidClient
{
    public class Client
    {
        public static Client CLIENT;

        private static IPEndPoint Origin = new IPEndPoint(IPAddress.Loopback, 57110);

        public const int DEFAULT_PORT = 57110;
        
        public Client() : this(IPAddress.Broadcast)
        {
        }

        public Client(string serverIP, int port = DEFAULT_PORT) : this(IPAddress.Parse(serverIP), port)
        {
        }

        public Client(IPAddress ipAddress, int port = DEFAULT_PORT)
        {
            endpoint = new IPEndPoint(ipAddress, port);
        }

        public void LeftButtonPressed() 
        {
            Msg("/leftbutton", new object[] { 0 });
        }

        public void LeftButtonReleased()
        {
            Msg("/leftbutton", new object[] { 1 });
        }

        public void RightButtonPressed()
        {
            Msg("/rightbutton", new object[] { 0 });
        }

        public void RightButtonReleased()
        {
            Msg("/rightbutton", new object[] { 1 });
        }

        public void MouseMoved(float x, float y)
        {
            Msg("/mouse", new object[] { 2, x, y });
        }

        private void Msg(string command, object[] parameters)
        {
            OscMessage msg = new OscMessage(Origin, command);

            foreach (object d in parameters)
            {
                if (d is int)
                {
                    msg.Append((Int32)d);
                }
                else if (d is float)
                {
                    msg.Append((Single)d);
                }
                else if (d is String)
                {
                    msg.Append((String)d);
                }
            }

            msg.Send(endpoint);
        }

        internal void KeyPressed(Key key)
        {
            if (KeyCommandGenerator.CONTROL_KEY_CODES.ContainsKey(key))
            {
                // MessageBox.Show(KeyCommandGenerator.CONTROL_KEY_CODES[key].ToString());
                KeyCommand((int)KeyState.PRESS, KeyCommandGenerator.CONTROL_KEY_CODES[key]);
            }
        }

        private void KeyCommand(int commandCode, int keyCode)
        {
            String keyString = new String(new char[] { (char)keyCode });
            Msg("/keyboard", new object[] { commandCode, keyCode, keyString });
        }

        internal void KeyReleased(Key key)
        {
            if (KeyCommandGenerator.CONTROL_KEY_CODES.ContainsKey(key))
            {
                KeyCommand((int) KeyState.RELEASE, KeyCommandGenerator.CONTROL_KEY_CODES[key]);
            }
        }

        internal void KeysTyped(string keys)
        {
            foreach (char c in keys)
            {
                KeyTyped(c);
            }
        }

        private void KeyTyped(char c)
        {
            IList<KeyCommand> commands = keyCommandGenerator.generateCommands(c);
            foreach (KeyCommand command in commands)
            {
                Msg("/keyboard", new object[] {(int) command.keyState, command.keyCode, command.s});
            }

        }

        public IPEndPoint endpoint { get; private set; }

        private KeyCommandGenerator keyCommandGenerator = new KeyCommandGenerator();
    }
}
