﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace RemoteDroidClient
{
    public partial class MainControlPage : PhoneApplicationPage
    {
        private Point oldPosition;

        private String oldText = "";

        public MainControlPage()
        {
            InitializeComponent();
        }

        private void keyboardButton_Click(object sender, RoutedEventArgs e)
        {
            keyboardInputBox.Text = "";
            keyboardInputBox.Focus();
        }

        private void mousePad_MouseEnter(object sender, MouseEventArgs e)
        {
            this.oldPosition = e.GetPosition(mousePad);
        }

        private void mousePad_MouseMove(object sender, MouseEventArgs e)
        {
            Point newPosition = e.GetPosition(this.mousePad);

            float dX = (float)(newPosition.X - oldPosition.X);
            float dY = (float)(newPosition.Y - oldPosition.Y);
            Client.CLIENT.MouseMoved(dX, dY);

            this.oldPosition = newPosition;
        }

        private void mousePad_Tap(object sender, GestureEventArgs e)
        {
            Client.CLIENT.LeftButtonPressed();
            Client.CLIENT.LeftButtonReleased();
        }

        private void rightButton_MouseEnter(object sender, MouseEventArgs e)
        {
            Client.CLIENT.RightButtonPressed();
        }

        private void rightButton_MouseLeave(object sender, MouseEventArgs e)
        {
            Client.CLIENT.RightButtonReleased();
        }

        private void leftButton_MouseEnter(object sender, MouseEventArgs e)
        {
            Client.CLIENT.LeftButtonPressed();

        }

        private void leftButton_MouseLeave(object sender, MouseEventArgs e)
        {
            Client.CLIENT.LeftButtonReleased();
        }

        private void keyboardInputBox_KeyUp(object sender, KeyEventArgs e)
        {
            Client.CLIENT.KeyReleased(e.Key);
        }

        private void keyboardInputBox_KeyDown(object sender, KeyEventArgs e)
        {
            Client.CLIENT.KeyPressed(e.Key);
        }

        private void keyboardInputBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            String newText = keyboardInputBox.Text;
            if (newText.Length > oldText.Length)
            {
                Client.CLIENT.KeysTyped(newText.Substring(oldText.Length));
                // MessageBox.Show();
            }
            oldText = newText;
        }


    }
}